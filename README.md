# Manual #

This repository is to define all `.proto` files used in different projects or stack. It's done for a purpose to automate generation of `pb` packages in the future.

### Updating/Adding new `.proto`: ###
1. Open a project folder you want to extend or edit, or create a new one. For example, `data-collector`.
2. Open existing `.proto` file or create new one in your case.
3. Make your changes.
4. Update/Clone [proto-go](https://bitbucket.org/yashserge/proto-go) in a parent folder.
5. Generate new `proto-go` version via command `$ protoc --go_out=../proto-go --go_opt=paths=source_relative --go-grpc_out=../proto-go --go-grpc_opt=paths=source_relative --swagger_out=../proto-go data-collector/*` in `proto` directory.
* If `protoc` causes some error, try:
  1. Run `$ vim ~/.bash_profile`
  2. Add
        ```
        export GO_PATH=~/go
        export PATH=$PATH:/$GO_PATH/bin
        ```
  3. Run `$ source ~/.bash_profile`
6. Commit changes in `proto` and `proto-go` repositories.

### Useful links are: ###
1. https://grpc.io/docs/languages/go/quickstart
2. https://grpc.io/docs/protoc-installation/
3. https://developers.google.com/protocol-buffers/docs/reference/go-generated
4. https://developers.google.com/protocol-buffers/docs/gotutorial
5. https://github.com/grpc-ecosystem/grpc-gateway
